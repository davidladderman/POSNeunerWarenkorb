<?php


require_once "./models/Cart.php";




$cart= new Cart();
$remove=false;
if(isset($_POST['submit'])){
    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    $cart-> remove($id);
    $remove = true;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" ;

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css">


    <style>
        .form .form-group {
            text-align: left;
        }
    </style>
    <title>Mein Warenkorb</title>
</head>
<body>
<div class="container">

    <div class="row">
        <div class="col-sm-10 form-group"><h1>Bücher</h1></div>
        <br>
        <div class="col-sm-2">
            <div class="btn btn-primary btn-block" >
                <a href="index2.php">Zurück</a>
            </div>
        </div>
    </div>
</div>
</body>


<?php
foreach ($cart->getBooksInCart() as $val) {
    ?>
    <div class="row">


        <div class="col-sm-12 "><b><?= $val->getBook()->getTitle() ?></b></div>
        <div class="col-sm-6 form-group">€ <?= $val->getPrice() ?></div>


        <div class="col-sm-2 form-group">



            <form action="cart.php" method="post">
                <input type="hidden" name="id" value="<?= $val->getBook()->getId() ?>">
                Menge:
                <input type="submit"
                       name="submit"
                       class="btn btn-primary btn-block"
                       value="Entfernen"/>
        </div>
        </form>


    </div>
    <?php
}
?>

</html>

