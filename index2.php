<?php



require_once "./models/Cart.php";


$bookList = Book::getAll();
$cart = new Cart();
if (isset($_POST['submit'])) {
    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    $stock = isset($_POST['anzahl']) ? $_POST['anzahl'] : 0;
    if ($stock > 0) {
        $book = Book::getById($id);
        $cart->add($book, $stock);
        echo "<script type='text/javascript'>alert('Buch hinzugefügt!');</script>";
    } else {
        echo "<script type='text/javascript'>alert('Kein Buch gefunden!');</script>";
    }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" ;

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css">


    <style>
        .form .form-group {
            text-align: left;
        }
    </style>
    <title>Bücher</title>
</head>
<body>
<div class="container">

    <div class="row">
        <div class="col-sm-10 form-group"><h1>Bücher</h1></div>
        <br>
        <div class="col-sm-2">
            <form action="cart.php" method="get">
                <input type="submit"
                       name="cart"
                       class="btn btn-primary btn-block"
                       value="Warenkorb "/>
            </form>
        </div>
    </div>
</div>
</body>


<?php
foreach ($bookList

         as $val) {

    ?>
    <div class="row">


        <div class="col-sm-12 "><b><?= $val->getTitle() ?></b></div>
        <div class="col-sm-6 form-group">€ <?= $val->getPrice() ?></div>


        <div class="col-sm-2 form-group">

            <?php
            if ($val->getStock() == 0){
                echo "Nicht auf Lager!";
            }else{
            ?>

            <form action="index2.php" method="post">
                <input type="hidden" name="id" value="<?= $val->getId() ?>">
                Menge:
                <select name="anzahl">
                    <?php
                    $counter = $val->getStock();

                    for ($c = 1; $c <= $counter; $c++) {
                        echo "<option value='$c'>" . $c . "</option>";
                    }

                    ?>
                </select>
                <input type="submit"
                       name="submit"
                       class="btn btn-primary btn-block"
                       value="Hinzufügen"/>
        </div>
        </form>
        <?php
        }
        ?>

    </div>
    <?php
}
?>

</html>
