<?php


require_once "Book.php";

class CartItem
{
    private $book;
    private $stock;

    /**
     * CartItem constructor.
     * @param $book
     * @param $stock
     */
    public function __construct($book, $stock)
    {
        $this->book = $book;
        $this->stock = $stock;
    }

    /**
     * @return mixed
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    public function getBookId(){
        return $this->book->getId();
    }

    public function addStock($stock) {
        $this->stock *= $stock;
    }

    public function getPrice(){
        return Book::getById($this->getBookId())->getPrice();
    }
}