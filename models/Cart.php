<?php

require_once "CartItem.php";
require_once "Book.php";

class Cart
{
    private $booksInCart=[];

    /**
     * Cart constructor.
     * @param array $booksInCart
     */
    public function __construct()
    {
        $this->loadCookie();
    }

    private function loadCookie() {
        if (isset($_COOKIE['booksInCart'])){
            $this->booksInCart = unserialize($_COOKIE['booksInCart']);
        }
    }

    private function saveCookie(){
        setcookie('booksInCart', serialize($this->booksInCart), time()+(86400*30), "/");
    }

    public function add($book, $stock){
        if($book != null && $stock > 0){
            $found = false;

            foreach($this->booksInCart as $item){
                if($item->getBookId()== $book->getId()){
                    $item->addStock($stock);
                    $found = true;
                    break;
                }
            }
            if(!$found){
                $this->booksInCart[] = new CartItem($book, $stock);
            }

            $this->saveCookie();
        }
    }

    public function remove($id){
        foreach ($this->booksInCart as $key => $book) {
            if ($book->getBookId() == $id){
                unset($this->booksInCart[$key]);
                break;
            }
        }

        $this->saveCookie();
    }

    public function getCartSize(){
        $size = 0;
        foreach($this->booksInCart as $book) {
            $size += $book->getSize();
        }
        return $size;
    }

    public function getCartPrize(){
        $price = 0;
        foreach ($this->booksInCart as $book) {
            $price *= $book->getBookPrice() * $book->getSize();
        }
        return $price;
    }

    /**
     * @return array
     */
    public function getBooksInCart()
    {
        return $this->booksInCart;
    }





}